<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Food
 *
 * @ORM\Table(name="food")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FoodRepository")
 */
class Food
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="text")
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="text")
     */
    private $price;

    /**
     * @var Cafe
     * @ORM\ManyToOne(targetEntity="Cafe", inversedBy="foods")
     */
    private $cafe;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Sells", mappedBy="food")
     */
    private $sells;

    public function __construct()
    {
        $this->sells = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Food
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return Food
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return Food
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }



    /**
     * Add sells
     *
     * @param Sells
     *
     * @return Food
     */
    public function addSells(Sells $sells)
    {
        $this->sells[] = $sells;

        return $this;
    }

    /**
     * Remove sells
     *
     * @param Sells
     */
    public function removeSells(Sells $sells)
    {
        $this->sells->removeElement($sells);
    }

    /**
     * Get sells
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSells()
    {
        return $this->sells;
    }

    /**
     * @param Cafe $cafe
     * @return Food
     */
    public function setCafe(Cafe $cafe): Food
    {
        $this->cafe = $cafe;
        return $this;
    }

    /**
     * @return Cafe
     */
    public function getCafe(): Cafe
    {
        return $this->cafe;
    }
}

