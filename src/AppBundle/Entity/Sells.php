<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Sells
 *
 * @ORM\Table(name="sells")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SellsRepository")
 */
class Sells
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var Food
     * @ORM\ManyToOne(targetEntity="Food", inversedBy="sells")
     */
    private $food;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Sells
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param Food $food
     * @return Sells
     */
    public function setFood($food)
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return Food
     */
    public function getFood()
    {
        return $this->food;
    }
}

