<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CafeController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $cafes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->findAll();

        $dishesByPlace = [];

        foreach ($cafes as $cafe){
            $dishes = $this
                ->getDoctrine()
                ->getRepository('AppBundle:Food')
                ->getMostPopularDishes($cafe, 2);
            $dishesByPlace[] = [
                'cafe' => $cafe,
                'dishes' => $dishes
            ];
        }


        return $this->render('@App/Cafe/index.html.twig', array(
            'dishesByPlaces' => $dishesByPlace
        ));
    }

    /**
     * @Route("/profile/{id}", requirements={"id" = "\d+"})
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function profileAction($id)
    {
        $cafes = $this
            ->getDoctrine()
            ->getRepository('AppBundle:Cafe')
            ->createQueryBuilder('c')
            ->select('c')
            ->leftJoin('c.foods', 't')
            ->andWhere('c.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        return $this->render('@App/Cafe/profile.html.twig', array(
            'cafes' => $cafes
        ));
    }

}
