<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cafe;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;


class LoadCafeData extends Fixture
{

    const KFC = "KFC";
    const STARBUCKS = "STARBUCKS";

    public function load(ObjectManager $manager)
    {
        $cafe = new Cafe();
        $cafe
            ->setName('KFC')
            ->setDescription('KFC — международная сеть ресторанов общественного питания, специализирующаяся на блюдах из курицы. Штаб-квартира компании располагается в городе Луисвилле в штате Кентукки')
            ->setImage('KFC_logo.png');

        $manager->persist($cafe);

        $cafe2 = new Cafe();
        $cafe2
            ->setName('STARBUCKS')
            ->setDescription('Starbucks Corporation — американская компания по продаже кофе и одноимённая сеть кофеен. Основана в Сиэтле в 1971 году. Управляющая компания — Starbucks Corporation.')
            ->setImage('starbucks-logo.png');

        $manager->persist($cafe2);

        $manager->flush();

        // other fixtures can get this object using the UserFixtures::ADMIN_USER_REFERENCE constant
        $this->addReference(self::STARBUCKS, $cafe);
        $this->addReference(self::KFC, $cafe2);
    }
}
