<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Food;
use AppBundle\Entity\Sells;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadSellsData extends Fixture
{

    public function load(ObjectManager $manager)
    {


        $sells1 = new Sells();
        $sells1
            ->setDate(new \DateTime("07-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_1));

        $manager->persist($sells1);

        $sells2 = new Sells();
        $sells2
            ->setDate(new \DateTime("08-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_2));

        $manager->persist($sells2);

        $sells3 = new Sells();
        $sells3
            ->setDate(new \DateTime("07-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_2));

        $manager->persist($sells3);

        $sells4 = new Sells();
        $sells4
            ->setDate(new \DateTime("07-08-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_3));

        $manager->persist($sells4);

        $sells5 = new Sells();
        $sells5
            ->setDate(new \DateTime("07-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_4));

        $manager->persist($sells5);

        $sells6 = new Sells();
        $sells6
            ->setDate(new \DateTime("14-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_4));

        $manager->persist($sells6);

        $sells7 = new Sells();
        $sells7
            ->setDate(new \DateTime("07-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_4));

        $manager->persist($sells7);

        $sells8 = new Sells();
        $sells8
            ->setDate(new \DateTime("27-05-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_5));

        $manager->persist($sells8);

        $sells9 = new Sells();
        $sells9
            ->setDate(new \DateTime("17-02-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_5));

        $manager->persist($sells9);

        $sells10 = new Sells();
        $sells10
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_6));

        $manager->persist($sells10);

        $sells11 = new Sells();
        $sells11
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_7));

        $manager->persist($sells11);

        $sells12 = new Sells();
        $sells12
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_7));

        $manager->persist($sells12);

        $sells13 = new Sells();
        $sells13
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_7));

        $manager->persist($sells13);

        $sells14 = new Sells();
        $sells14
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_8));

        $manager->persist($sells14);

        $sells15 = new Sells();
        $sells15
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_9));

        $manager->persist($sells15);

        $sells16 = new Sells();
        $sells16
            ->setDate(new \DateTime("17-12-2018"))
            ->setFood($this->getReference(LoadFoodData::FOOD_10));

        $manager->persist($sells16);


        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            LoadFoodData::class,
        );
    }
}
