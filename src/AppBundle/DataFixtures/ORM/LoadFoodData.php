<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Food;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFoodData extends Fixture
{
    const FOOD_1 = 'food1';
    const FOOD_2 = 'food2';
    const FOOD_3 = 'food3';
    const FOOD_4 = 'food4';
    const FOOD_5 = 'food5';
    const FOOD_6 = 'food6';
    const FOOD_7 = 'food7';
    const FOOD_8 = 'food8';
    const FOOD_9 = 'food9';
    const FOOD_10 = 'food10';

    public function load(ObjectManager $manager)
    {
        $dishes = ['Plov', 'Bar&b&que', 'hamburger', 'pizza', 'samsy', 'ratatui', 'hotdog', 'shaverma', 'fried potato', 'ravioli'];
        for ($i = 0; $i < 10; $i++) {
            if ($i % 2 === 0) {
                $cafe = LoadCafeData::KFC;
            } else {
                $cafe = LoadCafeData::STARBUCKS;
            }
            switch ($i) {
                case 0:
                    $foodConst = self::FOOD_1;
                    break;
                case 1:
                    $foodConst = self::FOOD_2;
                    break;
                case 2:
                    $foodConst = self::FOOD_3;
                    break;
                case 3:
                    $foodConst = self::FOOD_4;
                    break;
                case 4:
                    $foodConst = self::FOOD_5;
                    break;
                case 5:
                    $foodConst = self::FOOD_6;
                    break;
                case 6:
                    $foodConst = self::FOOD_7;
                    break;
                case 7:
                    $foodConst = self::FOOD_8;
                    break;
                case 8:
                    $foodConst = self::FOOD_9;
                    break;
                case 9:
                    $foodConst = self::FOOD_10;
                    break;

            }
            $food = new Food();
            $food
                ->setName($dishes[$i])
                ->setImage($i.'.jpg')
                ->setPrice(150 + $i * 10)
                ->setCafe($this->getReference($cafe));

            $manager->persist($food);
            $this->addReference($foodConst, $food);
        }

        $manager->flush();

    }

    public function getDependencies()
    {
        return array(
            LoadCafeData::class,
        );
    }
}
